import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Marco on 9/16/2017.
 */
public class MorseReaderNaive {

    public static final Map<String, String> MorseTranslator = initializeMorseMap();

    private static Map<String, String> initializeMorseMap() {
        Map<String, String> map = new HashMap<>();
        map.put(".-", "A");
        map.put("-.", "N");
        map.put("-----", "0");
        map.put("-...", "B");
        map.put("--.--", "Ñ");
        map.put(".----", "1");
        map.put("-.-.", "C");
        map.put("---", "O");
        map.put("..---", "2");
        map.put("----", "CH");
        map.put(".--.", "P");
        map.put("...--", "3");
        map.put("-..", "D");
        map.put("--.-", "Q");
        map.put("....-", "4");
        map.put(".", "E");
        map.put(".-.", "R");
        map.put(".....", "5");
        map.put("..-.", "F");
        map.put("...", "S");
        map.put("-....", "6");
        map.put("--.", "G");
        map.put("-", "T");
        map.put("--...", "7");
        map.put("....", "H");
        map.put("..-", "U");
        map.put("---..", "8");
        map.put("..", "I");
        map.put("...-", "V");
        map.put("----.", "9");
        map.put(".---", "J");
        map.put(".--", "W");
        map.put(".-.-.-", ".");
        map.put("-.-", "K");
        map.put("-..-", "X");
        map.put("-.-.--", ",");
        map.put(".-..", "L");
        map.put("-.--", "Y");
        map.put("..--..", "?");
        map.put("--", "M");
        map.put("--..", "Z");
        map.put(".-..-.", "\"");
        map.put("--..--", "!");
        map.put("SPACE", " ");
        return map;
    }

    public static final int CHAR_SEPARATOR = 4;
    public static final int LETTER_SEPARATOR = 8;
    public static final int WORD_SEPARATOR = 25;

    public static void main(String[] args) {
        byte[] img = null;
        try {
            img = extractBytes("./resources/trimmed.bmp");
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<String> morseMessage = getMessage(img);

        String message = translateMorse(morseMessage);


        System.out.println(message);
    }

    private static String translateMorse(List<String> morseMessage) {
        StringBuilder builder = new StringBuilder();
        for (String letter : morseMessage) {
            if (!MorseTranslator.containsKey(letter)) {
                throw new IllegalStateException("Cannot translate " + letter);
            }
            builder.append(MorseTranslator.get(letter));
        }
        return builder.toString();

    }

    private static List<String> getMessage(byte[] img) {
        List<String> letters = new LinkedList<>();
        String currentLetter = "";
        for (int it = 0, spaceCount = 0; it < img.length; it++) {
            if (img[it] == -1) {
                spaceCount++;
            } else {
                if (spaceCount >= WORD_SEPARATOR) {
                    letters.add(currentLetter);
                    letters.add("SPACE");
                    currentLetter = "";
                } else if (spaceCount >= CHAR_SEPARATOR) {
                    letters.add(currentLetter);
                    currentLetter = "";
                }

                spaceCount = 0;

                byte nextLetter = img[it + 1];
                if (nextLetter == -1) {
                    currentLetter += ".";
                } else {
                    currentLetter += "-";
                    it += 2;
                }
            }
        }
        return letters;
    }


    public static byte[] extractBytes(String ImageName) throws IOException {
        // open image
        File imgPath = new File(ImageName);
        BufferedImage bufferedImage = ImageIO.read(imgPath);

        // get DataBufferBytes from Raster
        WritableRaster raster = bufferedImage.getRaster();
        DataBufferByte data = (DataBufferByte) raster.getDataBuffer();

        byte[] array = data.getData();

        byte[] monobandArr = new byte[array.length / 3];

        for (int i = 0, monoIt = 0; i < array.length; i += 3, monoIt++) {
            monobandArr[monoIt] = array[i];

            if (i == raster.getWidth() - 1) {
                //Jump 2 lines
                i += raster.getWidth() * 3 * 2;
            }
        }

        return monobandArr;
    }
}
