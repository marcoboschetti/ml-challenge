/**
 * Created by Marco on 9/16/2017.
 */
public class TotalPrimeCombinationsV3 {

    public static final int MAX = 11500;

    public static void main(String[] args) {

        String totalSum = "540992328500517893750391624101174224";
        String pad = "673dbc07b736642ba64d2da5557056377674";

        for(int i = 0; i+1 < totalSum.length(); i+=2){
            byte[] padByte = hexStringToByteArray(pad.charAt(i)+""+pad.charAt(i+1));
            byte[] sumByte = hexStringToByteArray(totalSum.charAt(i)+""+totalSum.charAt(i+1));
            System.out.print(Character.valueOf((char) (padByte[0] ^ sumByte[0])));
        }
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
}
