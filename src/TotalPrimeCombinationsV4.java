import java.math.BigInteger;
import java.util.*;

/**
 * Created by Marco on 9/16/2017.
 */
public class TotalPrimeCombinationsV4 {

    public static final int MAX =  11500;

    public static void main(String[] args) {
        List<Long> firstMaxPrimes = new ArrayList<>();
        for (int i = 0; i <= MAX; i++) {
            if (isPrime(i)) {
                firstMaxPrimes.add((long) i);
            }
        }
        //  System.out.println("First "+firstMaxPrimes.size()+" primes counted");

        BigInteger total = BigInteger.ZERO;
        Map<Long, BigInteger> arr = new HashMap<>();
        Map<Long, BigInteger> tmpArr = new HashMap<>();
        List<Long> toRemove = new ArrayList<>();
        arr.put(firstMaxPrimes.get(0), BigInteger.valueOf(1));

        for (int primeIndex = 1; primeIndex < firstMaxPrimes.size(); primeIndex++) {
            Long prime = firstMaxPrimes.get(primeIndex);
            System.out.println("[" + prime + "]:" + arr.size());

            for (Map.Entry<Long, BigInteger> entry : arr.entrySet()) {
                Long i = entry.getKey();
                if (i + prime <= MAX) {
                    tmpArr.put((i + prime), entry.getValue());
                    total = total.add(entry.getValue());
                } else {
                    toRemove.add(i);
                }
            }
            for (Map.Entry<Long, BigInteger> tmpEntry : tmpArr.entrySet()) {
                Long key = tmpEntry.getKey();
                if(arr.containsKey(key)){
                    arr.put(key, arr.get(key).add(tmpArr.get(key)));
                    BigInteger sum = arr.get(key).add(tmpArr.get(key));
                    if(sum.compareTo(arr.get(key)) <= 0 || sum.compareTo(tmpArr.get(key)) <=0 ){
                        throw new IllegalStateException("Overflow! adding "+arr.get(key)+" and "+tmpArr.get(key));
                    }
                }else{
                    arr.put(key, tmpArr.get(key));
                }
            }
            if(arr.containsKey(prime)){
                arr.put(prime, arr.get(prime).add(BigInteger.ONE));
            }else{
                arr.put(prime,BigInteger.valueOf(1l));
            }

            //  java 8 lambdas, so cool (H)
            toRemove.forEach(arr::remove);
            toRemove.clear();
            tmpArr.clear();
        }

        System.out.println("Total(" + MAX + "):" + total);
        System.out.print(total + ",");

        String pad = "673dbc07b736642ba64d2da5557056377674";
        //GOTO V3
    }
    //Stackoverflow =)
    private static boolean isPrime(int num) {

        if (num < 2) return false;
        if (num == 2) return true;
        if (num % 2 == 0) return false;
        for (int i = 3; i * i <= num; i += 2)
            if (num % i == 0) return false;
        return true;
    }


}
