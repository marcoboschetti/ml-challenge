import java.math.BigInteger;
import java.util.*;

/**
 * Created by Marco on 9/16/2017.
 */
public class TotalPrimeCombinationsNaive {

    public static final int MAX = 11500;

    public static void main(String[] args) {

        BigInteger counter = BigInteger.ZERO;

        List<Integer> firstMaxPrimes = new LinkedList<>();
        for (int i = 0; i <= MAX; i++) {
            if (isPrime(i)) {
                firstMaxPrimes.add(i);
            }
        }
        System.out.println("First "+firstMaxPrimes.size()+" primes counted");

        Set<Addition> lastCombinations = new HashSet<>();
        for (int prime1 : firstMaxPrimes) {
            for (int prime2 : firstMaxPrimes) {
                if (prime1 > prime2 && prime1 + prime2 <= MAX) {
                    List<Integer> startingList = new LinkedList<>();
                    startingList.add(prime1);
                    startingList.add(prime2);
                    Addition newAddition = new Addition(startingList);
                    counter = counter.add(BigInteger.ONE);

                    lastCombinations.add(newAddition);
                }
            }
        }

        System.out.println("First "+counter+" combinations");
        BigInteger printRatio = BigInteger.valueOf(100000);

        boolean isNewAdded = true;
        Set<Addition> currentCombinations = new HashSet<>();
        while (isNewAdded) {
            isNewAdded = false;
            for (final Addition lastAdd : lastCombinations) {
                for (final int prime : firstMaxPrimes) {
                    if (!lastAdd.summands.contains(prime) && lastAdd.total + prime <= MAX) {
                        Addition newAddition = Addition.cloneAdding(lastAdd, prime);
                        if (!currentCombinations.contains(newAddition)) {
                            counter = counter.add(BigInteger.ONE);
                            currentCombinations.add(newAddition);
                            isNewAdded = true;

                            if(counter.mod(printRatio).equals(BigInteger.ZERO)){
                                System.out.println(counter);
                            }
                        }
                    }
                }
            }
            lastCombinations.clear();
            lastCombinations.addAll(currentCombinations);
            currentCombinations.clear();
        }

        System.out.println(counter.toString());
    }


    //Stackoverflow =)
    private static boolean isPrime(int num) {
        if (num < 2) return false;
        if (num == 2) return true;
        if (num % 2 == 0) return false;
        for (int i = 3; i * i <= num; i += 2)
            if (num % i == 0) return false;
        return true;
    }

    private static class Addition {
        List<Integer> summands;
        int total;

        public Addition(List<Integer> summands) {
            Collections.sort(summands);
            this.summands = summands;
            total = 0;
            for (Integer i : summands) {
                total += i;
            }
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Addition addition = (Addition) o;

            return summands != null ? summands.equals(addition.summands) : addition.summands == null;

        }

        @Override
        public int hashCode() {
            return summands != null ? summands.hashCode() : 0;
        }

        public static Addition cloneAdding(final Addition lastAdd, int prime) {
            List<Integer> newList = new LinkedList<>();
            newList.addAll(lastAdd.summands);
            Addition add = new Addition(newList);
            add.addSumand(prime);
            return add;
        }

        private void addSumand(int prime) {
            this.summands.add(prime);
            Collections.sort(this.summands);
            total += prime;
        }
    }
}
